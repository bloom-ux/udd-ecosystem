jQuery(function ($) {
	$("body").on("dblclick", ".blog-id-copy", (event) => {
		// Copiar ID del blog en pantalla de "Sitios" de la red.
		event.preventDefault();
		$(".copy-id-ok").removeClass("copy-id-ok");
		const $trigger = $(event.currentTarget);
		const blogId = $trigger.text();
		navigator.clipboard.writeText(blogId).then(() => {
			$trigger.addClass("copy-id-ok");
		});
	});
});
