#UDD Ecosystem

Una colección de fixes, mejoras y parches para sitios UDD.

## Módulos:

### src/normalize-utf8.php

Normaliza el uso de caracteres UTF-8 en rutas de archivos.

Soluciona problemas de archivos no encontrados por diferencias en codificación de sus nombres,
que generalmente se produce al subir archivos desde OSX (desnormalizados) a los sitios, donde
los archivos se guardan de forma normalizada.

### src/defaults.php

Define valores predeterminados para ciertas opciones y configuraciones de WordPress, como
zona horaria u otros.
