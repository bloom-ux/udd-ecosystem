<?php
/**
 * Definir defaults sensatos
 *
 * @package UDD_Ecosystem
 */

 namespace UDD_Ecosystem;

use WP_Error;
use Queulat\Helpers\Abstract_Admin;

define( 'UDD_ECOSYSTEM_DEFAULT_TIMEZONE', 'America/Santiago' );

/**
 * America/Santiago como zona horaria default
 */
add_filter(
	'default_option_timezone_string',
	function () {
		return UDD_ECOSYSTEM_DEFAULT_TIMEZONE;
	}
);

/**
 * America/Santiago como zona horaria si no se a configurado nada
 */
add_filter(
	'option_timezone_string',
	function ( $val ) {
		return $val ? $val : UDD_ECOSYSTEM_DEFAULT_TIMEZONE;
	}
);

/**
 * Cerrar comentarios en entradas antiguas
 */
add_filter(
	'pre_option_close_comments_for_old_posts',
	function () {
		return 1;
	}
);

/**
 * Por defecto, no permitir comentarios en nuevas entradas
 */
add_filter(
	'pre_option_default_comment_status',
	'__return_empty_string'
);

/**
 * Cerrar comentarios de entradas con más días de publicación
 */
add_filter(
	'pre_option_close_comments_days_old',
	function () {
		return 7;
	}
);

/**
 * No permitir comentarios.
 *
 * Afecta a los comentarios que se agregan con wp_handle_comment_submission()
 *
 * @see wp_handle_comment_submission()
 */
add_filter(
	'comments_open',
	'__return_false'
);

/**
 * Registrar cambios en configuraciones.
 *
 * Abarca las páginas creadas con Queulat\Abstract_Admint
 */
add_action(
	'queulat_abstract_admin_before_process_data',
	function ( array $sanitized_data, Abstract_Admin $admin_page ) {
		if ( ! function_exists( 'SimpleLogger' ) ) {
			return;
		}
		SimpleLogger()->info(
			'Modificación de opciones de configuración [{admin_page_id}]',
			array(
				'admin_page_id' => $admin_page->get_id(),
				'new_data' => $sanitized_data,
			)
		);
	},
	10,
	2
);

/**
 * Ocultar aviso de actualización de WordPress a usuarios
 */
add_action(
	'admin_head',
	function () {
		$user = wp_get_current_user();
		if ( ! is_super_admin( get_current_user_id() ) && ! str_contains( $user->user_email, 'bloom' ) ) {
			remove_action( 'admin_notices', 'update_nag', 3 );
		}
	}
);

/**
 * Loguear errores de envío de correo electrónico
 */
add_action(
	'wp_mail_failed',
	function ( WP_Error $error ) {
		if ( ! function_exists( 'SimpleLogger' ) ) {
			return;
		}
		SimpleLogger()->error(
			'Error al enviar correo electrónico',
			array(
				'error_code'    => $error->get_error_code(),
				'error_message' => $error->get_error_message(),
				'error_data'    => $error->get_error_data(),
			)
		);
	}
);

/**
 * Aumentar tiempo de espera para solicitudes HTTP
 */
add_filter(
	'http_request_args',
	function ( array $args, string $url ): array {
		if (
			str_contains( $url, 'admin-ajax.php' ) ||
			str_contains( $url, 'wp-cron.php' )
		) {
			return $args;
		}
		// Sólo modificar si el tiempo de espera es el default.
		if ( 5 === $args['timeout'] ) {
			$args['timeout'] = 10;
		}
		return $args;
	},
	10,
	2
);

/**
 * Deshabilitar reCAPTCHA en páginas de entradas
 *
 * Esto sólo tiene sentido si se está utilizando el plugin WP reCAPTCHA
 */
add_filter(
	'wp_recaptcha_do_scripts',
	function ( $do_recaptcha ) {
		if ( is_singular() ) {
			return false;
		}
		return $do_recaptcha;
	}
);

/**
 * Eliminar imágenes en base64
 */
add_filter(
	'wp_content_img_tag',
	function ( $filtered_image, $context, $attachment_id ): string {
		// Si es una imagen en base64, eliminar completamente.
		$re = '/ src=["\']image\/[a-zA-Z]{3,4};base64/';
		if ( preg_match( $re, $filtered_image ) ) {
			$filtered_image = '';
		}
		return $filtered_image;
	},
	10,
	3
);

/**
 * Corregir race condition en caché de alloptions
 *
 * @see https://core.trac.wordpress.org/ticket/31245
 * @param string $option Nombre de la opción.
 * @return void
 */
function maybe_clear_alloptions_cache( $option ) {
	if ( wp_installing() ) {
		return;
	}
	// alloptions should be cached at this point.
	$alloptions = wp_load_alloptions();

	// only if option is among alloptions.
	if ( isset( $alloptions[ $option ] ) ) {
		wp_cache_delete( 'alloptions', 'options' );
	}
}

add_action( 'added_option', '\UDD_Ecosystem\maybe_clear_alloptions_cache' );
add_action( 'updated_option', '\UDD_Ecosystem\maybe_clear_alloptions_cache' );
add_action( 'deleted_option', '\UDD_Ecosystem\maybe_clear_alloptions_cache' );
