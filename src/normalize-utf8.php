<?php
/**
 * Normalizar codificación de caracteres en archivos
 *
 * @package UDD_Ecosystem
 */

namespace UDD_Ecosystem;

use Normalizer;

/**
 * Normalizar la codificación de caracteres UTF-8
 * @param string $string String de entrada, posiblemente no normalizado
 * @return string String normalizado
 */
function normalize_utf8_encoding( string $string ) : string {
	if ( ! Normalizer::isNormalized( $string ) ) {
		$normalized = Normalizer::normalize( $string );
		return $normalized;
	}
	return $string;
}

add_filter( 'wp_get_attachment_url', '\UDD_Ecosystem\normalize_utf8_encoding' );
add_filter( 'wp_calculate_image_sizes', '\UDD_Ecosystem\normalize_utf8_encoding' );
add_filter( 'clean_url', '\UDD_Ecosystem\normalize_utf8_encoding' );

add_filter( 'wp_get_attachment_image_attributes', function( $attrs ) {
	foreach ( $attrs as $key => $val ) {
		$attrs[ $key ] = \UDD_Ecosystem\normalize_utf8_encoding( $val );
	}
	return $attrs;
} );

add_filter( 'wp_calculate_image_srcset', function( array $sources ) : array {
	return array_reduce(
		$sources,
		function( $carry, $source ) {
			$source['url'] = \UDD_Ecosystem\normalize_utf8_encoding( $source['url'] );
			$carry[] = $source;
			return $carry;
		},
		array()
	);
} );
