<?php
/**
 * Comando para migrar contenido en inglés de un sitio con Polylang a otro de la misma red
 *
 * @package UDD_Ecosystem\CLI
 */

namespace UDD_Ecosystem\CLI;

use WP_CLI;
use WP_Post;
use WP_Query;
use Queulat\Helpers\Arrays;

/**
 * Migrar contenido en inglés de un sitio con Polylang a otro en la misma red
 */
class Migrate_Polylang_Site {

	/**
	 * Conexión a la base de datos de WordPress
	 *
	 * @var \wpdb
	 */
	private $wpdb;

	/**
	 * IDs de imágenes usadas en cabeceras de páginas
	 *
	 * @var int[]|array
	 */
	private $hero_images_ids;

	/**
	 * Contenido en inglés a exportar
	 *
	 * @var WP_Post[]|array
	 */
	private $source_contents;

	/**
	 * Inicializar la clase
	 */
	public function __construct() {
		global $wpdb;
		$this->wpdb = $wpdb;
	}

	/**
	 * Información del sitio origen
	 *
	 * @var \WP_Site
	 */
	private $from_blog_info;

	/**
	 * Información del sitio destino
	 *
	 * @var \WP_Site
	 */
	private $to_blog_info;

	/**
	 * Migrar contenido en inglés de un sitio con Polylang a otro en la misma red
	 *
	 * ## OPTIONS
	 * <from-site>
	 * : URL del sitio origen
	 *
	 * <to-site>
	 * : URL del sitio destino
	 *
	 * @param array $args Argumentos de la línea de comandos.
	 * @param array $assoc_args Parámetros de la línea de comandos.
	 */
	public function __invoke( $args, $assoc_args ) {
		list( $from_site, $to_site ) = $args;
		$from_site = esc_url( trailingslashit( $from_site ) );
		$to_site   = esc_url( trailingslashit( $to_site ) );
		$this->from_blog_info = get_blog_details(
			array(
				'domain' => wp_parse_url( $from_site, PHP_URL_HOST ),
				'path' => wp_parse_url( $from_site, PHP_URL_PATH ),
			)
		);
		if ( empty( $this->from_blog_info ) ) {
			WP_CLI::error( 'No se encontró el sitio origen' );
		}

		$this->to_blog_info = get_blog_details(
			array(
				'domain' => wp_parse_url( $to_site, PHP_URL_HOST ),
				'path' => wp_parse_url( $to_site, PHP_URL_PATH ),
			)
		);
		if ( empty( $this->to_blog_info ) ) {
			WP_CLI::error( 'No se encontró el sitio destino' );
		}

		switch_to_blog( $this->from_blog_info->blog_id );

		// Obtener listado de contenidos que se deben exportar.
		$export_ids = $this->get_export_ids();

		if ( ! defined( 'IMPORT_DEBUG' ) ) {
			define( 'IMPORT_DEBUG', true );
		}

		$export_name = uniqid( 'export-' );
		WP_CLI::run_command(
			array( 'export' ),
			array(
				'max_file_size'    => '-1',
				'post_type'        => 'any',
				'post__in'         => implode( ',', $export_ids ),
				'with_attachments' => true,
				'filename_format'  => $export_name . '.xml',
			)
		);
		WP_CLI::line( 'Exportación de contenido en inglés completada' );

		// @todo: obtener info de menú de navegación.
		// $nav_menu_locations = get_nav_menu_locations();
		// $main_menu_english = wp_get_nav_menu_object( $nav_menu_locations['main__en'] );
		// $english_menu_items = wp_get_nav_menu_items( $main_menu_english->term_id );

		// @todo: configuración de customizer.

		WP_CLI::runcommand(
			"import ./{$export_name}.xml --url={$to_site} --authors=create"
		);
		WP_CLI::line( 'Importación de contenido en inglés completada' );

		restore_current_blog();
		switch_to_blog( (int) $this->to_blog_info->blog_id );

		// Actualizar referencias a imágenes en hero_media_custom_image.
		$this->fix_hero_media_ids();
		WP_CLI::line( 'Actualización de imágenes de cabecera completada' );

		// Actualizar links internos.
		$this->fix_internal_links();
		WP_CLI::line( 'Actualización de links internos completada' );

		// Eliminar archivo de exportación.
		WP_CLI::success( 'Contenido migrado exitosamente' );

		WP_CLI::confirm( '¿Desea eliminar el archivo de exportación?' );
		unlink( $export_name . '.xml' );
	}

	/**
	 * Actualizar links internos
	 *
	 * @return void
	 */
	private function fix_internal_links() {
		$original_blog_id = get_current_blog_id();
		switch_to_blog( $this->from_blog_info->blog_id );
		$permalinks = array_reduce(
			$this->get_source_contents(),
			function ( array $carry, WP_Post $item ): array {
				if ( 'attachment' === $item->post_type ) {
					return $carry;
				}
				$carry[] = get_permalink( $item->ID );
				return $carry;
			},
			array()
		);
		$new_permalinks = array_map(
			function ( $item ) {
				$from_blog_url = $this->from_blog_info->siteurl;
				$to_blog_url   = $this->to_blog_info->siteurl;
				return str_replace( $from_blog_url, $to_blog_url, $item );
			},
			$permalinks
		);
		switch_to_blog( $this->to_blog_info->blog_id );
		$target_contents = get_posts(
			array(
				'numberposts' => -1,
				'post_type'   => 'any',
				'post_status' => 'any',
			)
		);
		foreach ( $target_contents as $target_post ) {
			if ( 'attachment' === $target_post->post_type ) {
				continue;
			}
			$updated_content = str_replace( $permalinks, $new_permalinks, $target_post->post_content );
			if ( $updated_content !== $target_post->post_content ) {
				wp_update_post(
					array(
						'ID' => $target_post->ID,
						'post_content' => $updated_content,
					)
				);
			}
		}
		switch_to_blog( $original_blog_id );
	}

	/**
	 * Actualizar referencias a imágenes en hero_media_custom_image
	 *
	 * @return void
	 */
	private function fix_hero_media_ids() {
		// Obtener relación de post_id a warehouse_id, para posteriormente actualizar referencias
		// en hero_media_custom_image.
		$post_to_warehouse_ids = $this->get_post_to_warehouse_ids();
		$posts_with_hero_media = get_posts(
			array(
				'numberposts' => -1,
				'post_type'   => 'any',
				'post_status' => 'any',
				'meta_key'    => 'hero_media_custom_image',
			)
		);
		foreach ( $posts_with_hero_media as $post_with_hero ) {
			$original_hero_media_id = $post_with_hero->hero_media_custom_image;
			$warehouse_id = isset( $post_to_warehouse_ids[ $original_hero_media_id ]->warehouse_id ) ? $post_to_warehouse_ids[ $original_hero_media_id ]->warehouse_id : '';
			if ( ! $warehouse_id ) {
				continue;
			}
			$new_id = new WP_Query(
				array(
					'post_type' => 'attachment',
					'fields' => 'ids',
					'meta_key' => '_warehouse_id',
					'meta_value' => $warehouse_id,
					'post_status' => 'any',
				)
			);
			if ( ! $new_id->have_posts() ) {
				continue;
			}
			update_post_meta( $post_with_hero->ID, 'hero_media_custom_image', $new_id->post->ID );
		}
	}

	/**
	 * Obtener relación de post_id a warehouse_id
	 *
	 * Este dato sirve para posteriormente mapear las imágenes de cabecera de páginas
	 *
	 * @return array
	 */
	private function get_post_to_warehouse_ids() {
		$hero_images = $this->get_hero_images_ids();
		$format = implode( ',', array_fill( 0, count( $hero_images ), '%d' ) );
		return $this->wpdb->get_results(
			//phpcs:disable WordPress.DB.PreparedSQL.NotPrepared,WordPress.DB.PreparedSQL.InterpolatedNotPrepared
			$this->wpdb->prepare(
				"SELECT post_id as img_id, meta_value as warehouse_id FROM {$this->wpdb->postmeta} WHERE post_id IN ({$format}) AND meta_key = '_warehouse_id'",
				$hero_images
			),
			// phpcs:enable
			OBJECT_K
		);
	}

	/**
	 * Obtener ids de imágenes usadas en cabeceras de páginas
	 *
	 * @return int[]|array
	 */
	private function get_hero_images_ids(): array {
		if ( isset( $this->hero_images_ids ) ) {
			return $this->hero_images_ids;
		}
		//phpcs:ignore WordPress.DB.PreparedSQL.NotPrepared,WordPress.DB.PreparedSQL.InterpolatedNotPrepared
		$hero_images = $this->wpdb->get_col( "SELECT meta_value FROM {$this->wpdb->postmeta} WHERE meta_key = 'hero_media_custom_image' AND meta_value != ''" );
		$this->hero_images_ids = array_map( 'absint', $hero_images );
		return $this->hero_images_ids;
	}

	/**
	 * Obtener contenido en inglés a exportar
	 *
	 * @return WP_Post[]|array
	 */
	private function get_source_contents(): array {
		if ( isset( $this->source_contents ) ) {
			return $this->source_contents;
		}
		$this->source_contents = get_posts(
			array(
				'numberposts' => -1,
				'post_type'   => 'any',
				'post_status' => 'any',
				'lang'        => array( 'en_US', 'en' ),
			)
		);
		return $this->source_contents;
	}

	/**
	 * Obtener ids de contenido a exportar
	 *
	 * @return int[]|array
	 */
	private function get_export_ids() {
		$english_contents = $this->get_source_contents();

		$export_ids = wp_list_pluck( $english_contents, 'ID' );

		// Obtener ids de hero_media_custom_image.
		$hero_images = $this->get_hero_images_ids();

		// Obtener ids de imagenes en galerias u otros bloques.
		$export_ids = array_reduce(
			$english_contents,
			function ( array $carry, WP_Post $item ) {
				$post_blocks = parse_blocks( $item->post_content );
				$additional_ids = $this->find_export_ids( $post_blocks );
				if ( ! empty( $additional_ids ) ) {
					$carry = array_merge( $carry, $additional_ids );
				}
				return $carry;
			},
			$export_ids
		);
		$export_ids = array_map( 'absint', $export_ids );
		sort( $export_ids );
		$export_ids = array_unique( $export_ids );
		return $export_ids;
	}

	/**
	 * Encontrar ids de imágenes u otros attachments en contenidos de posts
	 *
	 * @param array $parsed_blocks Bloques parseados.
	 * @return int[]|array Lista de ids de attachments y otros elementos exportables
	 */
	private function find_export_ids( array $parsed_blocks ): array {
		$exportable_ids = $this->maybe_extract_gallery_ids( $parsed_blocks );
		$exportable_ids = array_merge(
			$exportable_ids,
			$this->maybe_extract_banner_ids( $parsed_blocks )
		);
		return $exportable_ids;
	}

	/**
	 * Obtener ids de imágenes en galerías
	 *
	 * @param array $blocks Bloques parseados.
	 * @return int[]|array Lista de ids de imágenes (vacío si no hay galerías)
	 */
	private function maybe_extract_gallery_ids( $blocks ): array {
		$exportable_ids = array();
		foreach ( $this->match_blocks( $blocks, array( 'blockName' => 'udd/content-gallery' ) ) as $block ) {
			foreach ( $block['attrs']['images'] as $image ) {
				if ( isset( $image['id'] ) ) {
					$exportable_ids[] = $image['id'];
				}
			}
		}
		return $exportable_ids;
	}

	/**
	 * Obtener ids de imágenes en banners
	 *
	 * @param array $blocks Bloques parseados.
	 * @return int[]|array Lista de ids de imágenes (vacío si no hay banners)
	 */
	private function maybe_extract_banner_ids( $blocks ) {
		$exportable_ids = array();
		foreach ( $this->match_blocks( $blocks, array( 'blockName' => 'udd/banners-multiple' ) ) as $block ) {
			foreach ( $block['attrs']['banners'] as $banner ) {
				if ( isset( $banner['image']['id'] ) ) {
					$exportable_ids[] = $banner['image']['id'];
				}
			}
		}
		return $exportable_ids;
	}

	/**
	 * Obtener un grupo de bloques en un pajar
	 *
	 * @param array $blocks Grupo de bloques.
	 * @param array $match_by Condiciones de filtro o búsqueda.
	 * @return array Grupo de coincidencias
	 */
	public function match_blocks( $blocks, $match_by ): array {
		$out = array();
		foreach ( $blocks as $block ) {
			$item_props = array();
			$block_flat = Arrays::flatten( $block );
			foreach ( $match_by as $prop => $val ) {
				if ( isset( $block_flat[ $prop ] ) ) {
					$item_props[ $prop ] = $block_flat[ $prop ];
				}
			}
			if ( $item_props === $match_by ) {
				$out[] = $block;
			}
			if ( ! empty( $block['innerBlocks'] ) ) {
				$children_match = $this->match_blocks( $block['innerBlocks'], $match_by );
				if ( $children_match ) {
					$out[] = $children_match;
				}
			}
		}
		return $this->flatten_blocks( $out );
	}

	/**
	 * Aplanar grupos de bloques
	 *
	 * @param array $blocks Bloques.
	 * @return array Bloques aplanados
	 */
	private function flatten_blocks( $blocks ) {
		if ( ! is_array( $blocks ) || count( $blocks ) === 0 ) {
			return array();
		}
		$result = array();
		foreach ( $blocks as $key => $block ) {
			if ( isset( $block['blockName'] ) ) {
				$result[] = $block;
				continue;
			}
			$children = $this->flatten_blocks( $block );
			$result   = array_merge( $result, $children );
		}
		return $result;
	}
}
