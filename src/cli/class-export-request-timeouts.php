<?php

namespace UDD_Ecosystem\CLI;

use SimpleHistory;

class Export_Request_Timeouts {

	/**
	 * Exportar información de peticiones HTTP que han fallado por timeout
	 *
	 * ## OPTIONS
	 *
	 * [--from=<date>]
	 * : Fecha de inicio en formato 'Y-m-d'
	 *
	 * @return never
	 */
	public function __invoke( $args, $assoc_args ) {
		global $wpdb;
		$sh_table      = $wpdb->prefix . SimpleHistory::DBTABLE;
		$sh_context    = $wpdb->prefix . SimpleHistory::DBTABLE_CONTEXTS;
		$from_date     = $assoc_args['from'] ?? null;
		$timeout_query = "SELECT {$sh_context}.history_id FROM {$sh_context} WHERE value LIKE '%timed out%'";
		$query_params  = array();
		$full_query    = "SELECT * FROM {$sh_table}
			LEFT JOIN {$sh_context} ON {$sh_table}.id = {$sh_context}.history_id
			WHERE {$sh_table}.id IN ({$timeout_query})
			AND {$sh_context}.key IN ('url', 'request', 'response')";
		if ( $from_date ) {
			$full_query .= " AND {$sh_table}.date >= %s ";
			$query_params[] = $from_date . ' 00:00:00';
		}
		$prepared = $wpdb->prepare( $full_query, $query_params );
		$results  = $wpdb->get_results( $prepared );
		$origin   = wp_parse_url( site_url(), PHP_URL_HOST );
		$maxmem   = 16 * 1024 * 1024;
		$fp       = fopen( "php://temp/maxmemory:{$maxmem}", 'r+' );
		$output   = array();
		foreach ( $results as $result ) {
			if ( empty( $output[$result->date] ) ) {
				$date = new \DateTime( $result->date, wp_timezone() );
				$output[$result->date] = array(
					'origin'  => $origin,
					'id'      => $result->id,
					'date'    => $date->format( 'Y-m-d' ),
					'time'    => $date->format( 'H:i:s' ),
					'message' => $result->message,
					'url'     => '',
					'error'   => ''
				);
			}
			if ( $result->key === 'request' && empty( $output[$result->date]['error'] )) {
				$output[$result->date]['error'] = json_decode( $result->value )->errors->http_request_failed[0];
			} elseif ( $result->key === 'url' ) {
				$output[$result->date]['url'] = $result->value;
			} elseif ( $result->key === 'response' && empty( $output[$result->date]['error'] ) ) {
				$json_value = json_decode( $result->value );
				$output[$result->date]['error'] = $json_value->errors->http_request_failed[0];
			}
		}
		foreach ( $output as $row ) {
			fputcsv( $fp, $row );
		}
		rewind( $fp );
		file_put_contents( "timeouts-{$origin}.csv", $fp );
		exit;
	}
}
