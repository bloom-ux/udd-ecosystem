<?php
/**
 * Mejoras a pantallas de administración
 *
 * @package UDD_Ecosystem
 */

namespace UDD_Ecosystem;

/**
 * Agregar columna con ID del blog a listado de sitios de la red
 *
 * @param array $cols Columnas de tabla de administración
 * @return array Columnas con blog ID
 */
function add_blog_id_column( array $cols ): array {
	$first_piece = array_slice( $cols, 0, 2 );
	$second_piece = array_slice( $cols, 2 );
	$first_piece['blog_id'] = 'ID';
	return $first_piece + $second_piece;
}

add_filter( 'wpmu_blogs_columns', '\UDD_Ecosystem\add_blog_id_column' );

/**
 * Añadir dato de blog ID a listado de sitios de la red.
 *
 * @param string $column_name Slug de la columna
 * @param int    $blog_id ID del blog
 * @return void
 */
function blog_id_column( string $column_name, int $blog_id ) {
	if ( 'blog_id' !== $column_name ) {
		return;
	}
	echo "<span class='blog-id-copy' title='Doble click para copiar el ID del blog'>";
	echo esc_html( $blog_id );
	echo '</span>';
}

add_action( 'manage_sites_custom_column', '\UDD_Ecosystem\blog_id_column', 10, 2 );

/**
 * Encolar CSS y JS para administración
 */
function enqueue_admin_assets() {
	wp_enqueue_style(
		'udd-ecosystem-admins-style',
		plugins_url( '../css/admin.css', __FILE__ ),
		array(),
		false,
		'all'
	);
	wp_enqueue_script(
		'udd-ecosystem-admin',
		plugins_url( '../js/admin.js', __FILE__ ),
		array( 'jquery-core' ),
		false,
		true
	);
}

add_action( 'admin_enqueue_scripts', '\UDD_Ecosystem\enqueue_admin_assets' );
