<?php
/**
 * Plugin Name: Ecosistema UDD
 * Plugin URI: https://gitlab.com/bloom-ux/udd-ecosystem/
 * Description: Una colección de mejoras y configuraciones específicas para sitios UDD.
 * Version: 0.1.0
 * Author: Bloom User Experience
 * Author URI: https://www.bloom-ux.com
 * License: GPL-3.0-or-later
 *
 * @package UDD_Ecosystem
 */

namespace UDD_Ecosystem;

use UDD_Ecosystem\CLI\Migrate_Polylang_Site;
use UDD_Ecosystem\CLI\Export_Request_Timeouts;

// Normalizar caracteres en archivos.
require_once __DIR__ . '/src/normalize-utf8.php';

// Defaults.
require_once __DIR__ . '/src/defaults.php';

// Mejoras en el admin.
require_once __DIR__ . '/src/admin-tweaks.php';

// Herramientas para línea de comandos.
if ( is_callable( array( 'WP_CLI', 'add_command' ) ) ) {
	require_once __DIR__ . '/src/cli/class-export-request-timeouts.php';
	\WP_CLI::add_command( 'udd export-request-timeouts', new Export_Request_Timeouts() );
	require_once __DIR__ . '/src/cli/class-migrate-polylang-site.php';
	\WP_CLI::add_command( 'udd migrate-polylang-site', new Migrate_Polylang_Site() );
}

/**
 * Eliminar caracteres especiales en nombres de archivos
 *
 * @param string $input Nombre de archivo original.
 * @return string Nombre de archivo sanitizado
 */
function sanitize_file_name( string $input ): string {
	$input                    = normalize_utf8_encoding( $input );
	$input                    = mb_convert_case( $input, MB_CASE_LOWER, mb_detect_encoding( $input ) );
	$filename                 = pathinfo( $input, PATHINFO_FILENAME );
	$sanitized_filename       = sanitize_title_with_dashes( remove_accents( $filename ) );
	$sanitized_with_extension = str_replace( $filename, $sanitized_filename, $input );
	return $sanitized_with_extension;
}

add_filter( 'sanitize_file_name', '\UDD_Ecosystem\sanitize_file_name' );
